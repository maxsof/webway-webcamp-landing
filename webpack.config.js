require("dotenv").config();

const path = require("path");
const isProduction = process.env.NODE_ENV === "production" ? true : false;

const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlLayoutPlugin = require("html-layout-plugin");
const HtmlBeautifyPlugin = require("html-beautify-webpack-plugin");

module.exports = {
  mode: process.env.NODE_ENV,
  entry: {
    index: [
      "./src/assets/js/index.js",
      "./src/assets/less/index.less",
      "./src/components/welcome/index.less",
      "./src/components/carefree/index.less",
      "./src/components/example/index.less",
      "./src/components/bubble/index.less",
    ],
    price: [
      "./src/assets/js/index.js",
      "./src/assets/less/index.less",
      "./src/components/version/index.less",
      "./src/components/tariff/index.less",
      "./src/components/period/index.less",
      "./src/components/tech/index.less",
      "./src/components/notebook/index.less",
    ],
    signup: [
      "./src/assets/js/index.js",
      "./src/assets/less/index.less",
      "./src/components/auth/index.less",
    ],
  },
  output: {
    filename: "js/[name].min.js",
    path: path.resolve(__dirname, "build"),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "babel-loader",
      }, {
        test: /\.(le|c)ss$/,
        use: [
          isProduction ? MiniCssExtractPlugin.loader : "style-loader",
          "css-loader",
          "postcss-loader",
          "less-loader",
        ],
      }, {
        test: /\.(png|jpg|svg,)$/,
        loader: "file-loader",
      }, {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        loader: "file-loader",
      },
    ],
  },
  stats: "errors-only",
  devServer: {
    stats: "errors-only",
    host: process.env.HOST,
    port: process.env.PORT,
  },
  plugins: [
    new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: isProduction ? ["**/*"] : [] }),
    new CopyWebpackPlugin([{ from: "./src/static/" }]),
    new MiniCssExtractPlugin({ filename: isProduction ? "css/[name].min.css": "[name].css" }),
    new HtmlWebpackPlugin({
      layout: path.join(__dirname, "src/layouts/default.html"),
      template: "./src/pages/index.html",
      filename: "index.html",
      chunks: ["index"],
    }),
    new HtmlWebpackPlugin({
      layout: path.join(__dirname, "src/layouts/default.html"),
      template: "./src/pages/price.html",
      filename: "price.html",
      chunks: ["price"],
    }),
    new HtmlWebpackPlugin({
      layout: path.join(__dirname, "src/layouts/default.html"),
      template: "./src/pages/signup.html",
      filename: "signup.html",
      chunks: ["signup"],
    }),
    new HtmlLayoutPlugin(),
    new HtmlBeautifyPlugin({
      config: {
        html: {
          indent_size: 2,
          preserve_newlines: true,
          indent_inner_html: true,
        },
      },
      replace: ["type=\"text/javascript\""],
    }),
  ],
};
